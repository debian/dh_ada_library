# Helper for debian/rules in Debian packages with Ada sources.

#  Copyright (C) 2012-2024 Nicolas Boulenguez <nicolas@debian.org>

#  This program is free software: you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#  This program is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#  General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.

# Typical use:
#
# DEB_BUILD_MAINT_OPTIONS := hardening=+all
# DEB_LDFLAGS_MAINT_APPEND := -Wl,--no-undefined -Wl,--no-copy-dt-needed-entries -Wl,--no-allow-shlib-undefined
# DEB_ADAFLAGS_MAINT_APPEND := -gnatwa -Wall
# DPKG_EXPORT_BUILDFLAGS := 1
# include /usr/share/dpkg/buildflags.mk
# include $(wildcard /usr/share/ada/packaging.mk)
# # The wilcard is useful when Build-Depends-Indep does not contain gnat.

# DPKG_EXPORT_BUILDFLAGS and buildflags.mk are recommended by
# dpkg-buildflags(1). Even pure Ada code will use at least LDFLAGS.

# Beware to #872381 if other snippets are needed or buildopts.mk grows.
# For now, it seems to support double inclusion (by debian/rules).
include /usr/share/dpkg/buildopts.mk

######################################################################
# Convenient constants.
# The directory paths contain neither leading nor trailing slash.

# DEB_ADA_SOURCE_DIR
#   foo/*.{adb,ads,c,h...}

# DEB_LIB_DIR
#   libfoo.{a,so,so.SO_VERSION}
#   Nothing specific to Ada, but convenient.

# DEB_ADA_LIB_INFO_DIR
#   foo/*.ali

# DEB_GNAT_PROJECT_DIR
#   foo.gpr

# DEB_GNAT_VERSION
#   The GNAT version is similar `gcc -dumversion`.
#   On Debian, it is the major comporent of -dumpfullversion.
#   The full version is portable, the version is not.

#   It is important for packages linking objects built from Ada and
#   other languages.  For example, C must be compiled with
#   gcc-DEB_GNAT_VERSION.  See /usr/share/doc/gnat-base/README.gnat.

#   Note: unlike with C/C++/..., two gnat-DEB_GNAT_VERSION packages
#   are not coinstallable because both provide the /usr/bin/gnatmake
#   -> gnatmake-DEB_GNAT_VERSION symbolic link.

# name_LIB_PKG
# name_SO_VERSION

#   For each architecture-dependent package in debian/control, named
#   libNAME-dev, expanding the ada:Depends and ada:Provides
#   substitution variables, the Make variables above are set.
#   The library name is lowercase, with dash replaced by underscore.

# Unfortunately, some upstream hardcode exotic schemes instead of
# libfoo.so.$(foo_so_version), so SO_NAME here would be confusing.

$(foreach line,$(shell dh_ada_library --export-versions),\
  $(eval $(line)))

######################################################################
# Ada build flags

# The following defines ADAFLAGS, basically copying the value for
# CFLAGS computed by dpkg-buildflags, except that
# hardening=+format qa=+bug cause a (deserved) GNAT warning
# hardening=+stackclash is currently broken on armel armhf
#   (https://gcc.gnu.org/bugzilla/show_bug.cgi?id=118164).
# Both changes can be circumvented by DEB_ADAFLAGS_MAINT_STRIP if ever
# necessary.

# dpkg-buildflags(1) recommends to set DPKG_EXPORT_BUILDFLAGS before
# including buildflags.mk and/or this snippet (or export the needed
# flags manually).

# Reproducibility issue:
# In libxmlada/22.0.0-3, gnatmake calls gcc with -gnatO /build/path/foo.o.
# This results in unreproducible debug information.
# This should ideally be fixed by BUILD_PATH_PREFIX_MAP or relative paths.
# Gprbuild generates -gnate[cm] switches with a counter, but
# parallelism makes the enumeration order unreproducible.

# Implementation details:
# * actual DEB_CFLAGS_* values must not influence ADAFLAGS
# * DEB_ADAFLAGS_* must, even if defined in Make and not exported yet.
# * DEB_ADAFLAGS_SET has two completely different meaning when
#   undefined or empty.
# * modifying DEB_ADAFLAGS_* within Make may lead to surprises
#   depending on how and if debian/rules defines them.
# * Compute once (:=) but do not attempt lazy evaluation. The value
#   will most probably be used by export right after its definition.

ADAFLAGS := $(shell \
  $(foreach v,DEB_ADAFLAGS_SET     DEB_ADAFLAGS_MAINT_SET \
              DEB_ADAFLAGS_PREPEND DEB_ADAFLAGS_MAINT_PREPEND \
                                   DEB_ADAFLAGS_MAINT_STRIP \
                                   DEB_ADAFLAGS_MAINT_APPEND \
              DEB_BUILD_OPTIONS    DEB_BUILD_MAINT_OPTIONS, \
    $(if $(filter undefined,$(origin $v)),\
      unset $(v:DEB_ADA%=DEB_C%),\
      export $(v:DEB_ADA%=DEB_C%)='$($v)') &&)\
  DEB_CFLAGS_STRIP='$(DEB_ADAFLAGS_STRIP) -Wformat -Werror=format-security \
    -Werror=implicit-function-declaration -fstack-clash-protection ' \
  DEB_CFLAGS_APPEND='$(DEB_ADAFLAGS_APPEND) -gno-record-gcc-switches' \
  dpkg-buildflags --get CFLAGS)

ifdef DPKG_EXPORT_BUILDFLAGS
  export ADAFLAGS
endif

######################################################################
# Race conditions

# Passing the right -jN option from DEB_BUILD_OPTIONS to the right
# tool (gnatmake, gprbuild, sphinx...) can increase performance, that
# is why dh_auto_build tries to do it.

# However, Ada upstreams often consider Makefile as a simple driver
# for such tools.  Allowing parallelism in such a Makefile leads to
# race conditions, because $tool assumes that no other instance is
# processing the same files.

# For example, 'dh_auto_build -- FOO=bar' is usally better than
# '$(MAKE) Foo=bar', but should be avoided in the situation described
# above.

######################################################################
# gnatmake

# -R
# Avoid lintian warning about setting an explicit library runpath.
# http://wiki.debian.org/RpathIssue

# -eS
# Tell gnatmake to echo commands to stdout instead of stderr, avoiding
# buildds thinking it is inactive and killing it.

GNATMAKEFLAGS := \
  $(if $(filter terse,$(DEB_BUILD_OPTIONS)),,-v) \
  $(DEB_BUILD_OPTION_PARALLEL:%=-j%) \
  -R \
  -eS \
  # EOL

# You may be interested in
# -s  recompile if compilation switches have changed
#     (bad default because of interactions between -amxs and standard library)
# -we handle warnings as errors

######################################################################
# GNAT projects.
# Ignore this section if gprbuild is not listed in Build-Depends(-Arch).

# When gprconfig is run explicitly, it creates default.cgpr that
# other gpr tools read later.
#   gprconfig --batch $(GPRCONFIGFLAGS)
#   gprbuild foo.gpr
# If it finds no .cgpr, gprbuild runs gprconfig and creates a
# temporary auto.cgpr.
#   gprbuild foo.cgpr -kargs $(GPRCONFIGFLAGS)

# If the sources are not only Ada, a consistent Application Binary
# Interface is only guaranteed if all objects are built with GCC
# compilers with the same major version (note however that the GCC
# policy is to announce any ABI break and make them rare).

# To stay on the safe side, select the same GCC version for all
# available GCC compilers. There is no point in restricting the list
# to languages that the current package actually needs, but available
# compilers at least include those listed in Build-Depends.

# -v writes to standard error, leading tools like autopkgtest to
# report a failure

GPRCONFIGFLAGS := --config=Ada
ifneq (,$(wildcard /usr/bin/gcc-$(DEB_GNAT_VERSION)))
  GPRCONFIGFLAGS += --config=C,,,,gcc-$(DEB_GNAT_VERSION)
endif
ifneq (,$(wildcard /usr/bin/g++-$(DEB_GNAT_VERSION)))
  GPRCONFIGFLAGS += --config=C++,,,,g++-$(DEB_GNAT_VERSION)
endif
ifneq (,$(wildcard /usr/bin/gfortran-$(DEB_GNAT_VERSION)))
  GPRCONFIGFLAGS += --config=Fortran,,,,gfortran-$(DEB_GNAT_VERSION)
endif

# Options for the gprbuild tool, driving compilations.
# Similar to gnatmake above, except that -eS is the default.

# Some GNAT projects, especially when driven by a Makefile, offer a
# parallelism configuration that interacts in obscure ways with -j.
# DEB_GPRBUILDFLAGS_NO_J was once introduced, but was a source of
# complexity and unneeded restrictions.
ifdef DEB_GPRBUILDFLAGS_NO_J
  $(error DEB_GPRBUILDFLAGS_NO_J in debian/rules is deprecated. \
    Please replace it where appropriate with constructs like \
    $$(GPRBUILDFLAGS:-j%=) $$(DEB_BUILD_OPTION_PARALLEL:%=-XPROCESSORS=%))
endif

GPRBUILDFLAGS := \
  -R \
  $(if $(filter terse,$(DEB_BUILD_OPTIONS)),,-v) \
  $(DEB_BUILD_OPTION_PARALLEL:%=-j%) \
  # EOL

# Gprinstall provides no staged installs (DESTDIR), so we are limited
# to --prefix, which also influences installation of
# architecture-independent data like documentation and examples
# (Install'Artifacts in the project).

# Most such files need the build-indep dependencies target and have to
# be stripped by patches anyway, but for the rare wanted artifacts,
# add usr/ to --prefix and remove it from --*-subdir.

common_GPRINSTALLFLAGS := \
  $(if $(filter terse,$(DEB_BUILD_OPTIONS)),,-v) \
  -f \
  -p \
  --no-build-var \
  --no-manifest \
  --prefix=debian/tmp/usr \
  # EOL

# For standard projects building executables, not a library.

standard_GPRINSTALLFLAGS := \
  $(common_GPRINSTALLFLAGS) \
  --mode=usage \
  --exec-subdir=bin \
  # EOL

# Installation of library projects requires several runs of gprinstall
# with different options.

# For static archives, --mode=usage would prevent installation of
# sources and .ali, but does nothing with the following options
# (https://github.com/AdaCore/gprbuild/issues/117)
# For now, install them under an explicitly absurd directory.

# dh_ada_library knows that it should ignore this specific
# directory. If you are not using it, please add it to
# debian/not-installed.

# gprinstall a.gpr $(static_GPRINSTALLFLAGS) -Xlibrary_kind=static
# gprinstall b.gpr $(static_GPRINSTALLFLAGS) -Xlibrary_kind=static

static_GPRINSTALLFLAGS := \
  $(common_GPRINSTALLFLAGS) \
  --lib-subdir=$(DEB_LIB_DIR:usr/%=%) \
  --no-project \
  --ali-subdir=unwantedly_gprinstalled \
  --sources-subdir=unwantedly_gprinstalled \
  # EOL

# For shared libraries, distinct options are necessary.
# gprinstall a.gpr $(call shared_GPRINSTALLFLAGS,a) -Xlibrary_kind=relocatable
# gprinstall b.gpr $(call shared_GPRINSTALLFLAGS,b) -Xlibrary_kind=relocatable

# --install-name=foo would install ali and sources nicely, but also
# add a add a subdirectory and two more symbolic links in libdir.

shared_GPRINSTALLFLAGS = \
  $(common_GPRINSTALLFLAGS) \
  --lib-subdir=$(DEB_LIB_DIR:usr/%=%) \
  --link-lib-subdir=$(DEB_LIB_DIR:usr/%=%) \
  --project-subdir=$(DEB_GNAT_PROJECT_DIR:usr/%=%) \
  --ali-subdir=$(DEB_ADA_LIB_INFO_DIR:usr/%=%)/$1 \
  --sources-subdir=$(DEB_ADA_SOURCE_DIR:usr/%=%)/$1 \
  # EOL

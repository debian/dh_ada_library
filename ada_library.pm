# dh_ada_library - help packaging Ada libraries for Debian
# Insert dh_ada_library into the dh sequence.
use strict;
use warnings;
insert_after( 'dh_lintian', 'dh_ada_library' );
1;

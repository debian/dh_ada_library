# -- Driven by debian/rules

include /usr/share/dpkg/pkg-info.mk

all: obj/dh_ada_library.1

obj/%.1: %.pod | obj
	pod2man --utf8 -cDebhelper '-r$(DEB_VERSION)' $< $@

obj:
	mkdir $@

clean:
	rm -fr obj/
	find . -name '*~' -a -type f -delete

# -- Useful during development

dev: c-dh_ada_library obj/tidy-dh_ada_library obj/critic-dh_ada_library \
     all \
     c-ada_library.pm obj/tidy-ada_library.pm obj/critic-ada_library.pm

c-%:
	perl -c $*

# apt install perltidy:
obj/tidy-%: %
	perltidy $< -st | diff -u $< -

no_critic = Modules::RequireVersionVar
obj/critic-ada_library.pm: no_critic += Modules::RequireExplicitPackage

# apt install libperl-critic-perl:
obj/critic-%: % Makefile | obj
	perlcritic -1 --verbose=11 $(no_critic:%=--exclude=%) $<
	touch $@
